(ns tictactoe.board)
(def instructional-board (into [] (range 0 10)))

(defn open-squares [board]
  (loop [i 1
         squares []]
    (let [new-squares (if (= (nth board i) " ") (into [] (concat squares [i])) squares)]
      (if (= i 9) new-squares
                  (recur (inc i) new-squares)))))

(defn get-row [board, row-number]
  (let [start-index (+ (* (- row-number 1) 3) 1)
        end-index (+ start-index 3)
        row (subvec board start-index end-index)
        final-row (apply conj [" "] row)]
        final-row
    )
  )
(defn print-row
  [board, row-number]
  (let [row (subvec (get-row board row-number) 1 4)
        row-string (clojure.string/join " | " row)
        ]
    (print "" row-string "\n")
    (flush)))

(defn print-board
  [board]
  (doseq [i (range 1 4)]
    (do
      (if (> i 2) (print-row board i)
        (do
          (print-row board i)
          (print "-----------\n")
          (flush))))))

(defn get-column [board, column-number]
  (let [column (vec (repeat 4 " "))]
    (assoc column 1 (nth board column-number) 2 (nth board (+ column-number 3)) 3 (nth board (+ column-number 6)))))

(defn get-diagonal [board, diagonal-number]
  (let [diagonal (vec (repeat 4 " "))]
    (cond (= diagonal-number 1) (assoc diagonal 1 (nth board 1) 2 (nth board 5) 3 (nth board 9))
          (= diagonal-number 2) (assoc diagonal 1 (nth board 3) 2 (nth board 5) 3 (nth board 7))
          :else diagonal
    )))

(defn get-all [board get]
  (into [] (for [i (range 0 4)] (if (= i 0) (vec (repeat 4 " "))(get board i)))))

(defn saturated? [triple]
  (cond (= (nth triple 1) (nth triple 2) (nth triple 3)) (nth triple 1)
        :else " "))

(defn winner? [board]
  (let [rows (get-all board get-row)
        columns (get-all board get-column)
        diagonals (get-all board get-diagonal)
        combinations (into [] (concat rows columns diagonals))
        result (reduce (fn [a i] (if (= a " ") (saturated? i) a)) " " combinations)]
        result
  ))
