(ns tictactoe.computer
  (:require
    [tictactoe.board :refer :all])
    )

(def opponent { "X" "O"
                "O" "X"})
(def prioritized-move-options [5 1 3 7 9 2 4 6 8])

(defn find-winner [me board]
  (let [squares (open-squares board)]
    (loop [remaining-squares squares]
      (let [winner (winner? (assoc board (nth remaining-squares 0) me))]
      (cond
        (= me winner) (nth remaining-squares 0)
        (= 1 (count remaining-squares)) 0
        :else (recur (subvec remaining-squares 1)))))))

(defn find-blocker [me board]
  (find-winner (opponent me) board))

(defn is-fork? [me board cell]
  (let [move-option (assoc board cell me)
        next-moves (open-squares move-option)
        my-wins (reduce (fn [a i] (if (= me (winner? (assoc move-option i me))) (inc a) a)) 0 next-moves)
        their-wins (reduce (fn [a i] (if (= (opponent me) (winner? (assoc move-option i (opponent me)))) (inc a) a)) 0 next-moves)]
    (and (> my-wins 1) (< their-wins 1))))

(defn is-pre-fork? [me board cell]
  (let [move-option (assoc board cell me)
        next-moves (open-squares move-option)
        o (opponent me)]
    (reduce (fn [a i] (or a (is-fork? o move-option i))) false next-moves)))

(defn find-fork [me board]
  (loop [options (vec (filter (set (open-squares board)) prioritized-move-options))]
    (let [option (nth options 0)
          pre-fork (is-pre-fork? me board option)]
    (if (not pre-fork)
      (nth options 0)
      (recur (subvec options 1))))))

(defn computer-move [me, board]
  (let [algorithm [find-winner find-blocker find-fork]]
      (reduce (fn [a i] (if (= 0 a) (i me board) a)) 0 algorithm)))
