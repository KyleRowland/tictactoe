(ns tictactoe.core
  (:require [tictactoe.board :refer :all])
  (:require [tictactoe.computer :refer :all])
  (:require [clojure.string :as str]))


(import 'java.io.BufferedReader
        'java.io.InputStreamReader)

(defn set-players
  ([which type] {which type})
  ([players which type] (assoc players which type)))

(def string-map {:H "Human" :C "Computer"})

(defn acceptance-string [players]
  (str  "Fantastic - X will be played by a " (string-map (:X players))
        " and O will be played by a " (string-map (:O players))
        "! Enjoy!"))

(defn get-player-type []
  (let [player-key (keyword (str/upper-case (read-line)))]
    (cond
      (or (= :H player-key) (= :C player-key)) player-key
      :else (do
       (println "Players must be (H)uman or (C)omputer.  Have a nice day!")
       (System/exit 0)
       ))))

(defn prompt-player [players, number, letter]
  (print "Player" number "- (H)uman or (C)omputer: ")
  (flush)
  (set-players players letter (get-player-type)))

(defn as-integer [s]
  (try (java.lang.Integer/parseInt s)
  (catch NumberFormatException e 0)))

(defn no-squares [board]
  (= (count (open-squares board)) 0))

(def player-letter-map {1 "X"
                        2 "O"})

(defn prompt-cell-selection [player-number, board]
  (let [player-letter (player-letter-map player-number)]
    (println "Player" player-number "(" player-letter "): Choose a square number ('Q' to quit).\n")
    (print-board instructional-board)
    (println)
    (print-board board)
    (println "\n")
    (print "] ")(flush)
    (as-integer(read-line))))

(defn compute-cell-selection [player-number, board]
  (let [player-letter (player-letter-map player-number)]
    (print-board board)
    (println "Player" player-number "(" player-letter "): Computer is choosing a square...")
    (computer-move player-letter board)))

(defn player-winner [board] (not (= " " (winner? board))))

(defn get-cell-selection
  ([players player-number board] (get-cell-selection players player-number board false))
  ([players player-number board quit]
  (cond (not (or (no-squares board) (player-winner board) quit))
  (let [player (players (keyword (player-letter-map player-number)))]
    (cond
      (= player :H) (prompt-cell-selection player-number board)
      (= player :C) (compute-cell-selection player-number board))))))

(defn make-move [cell, letter, board]
  (cond
        (or (nil? cell) (player-winner board)) board
        (not (= (nth board cell) " ")) (do (println "\nCell already taken - move forfeited!\n") board)
        :else (if (and (> cell 0) (= (nth board cell) " ")) (assoc board cell letter) board)))

(defn is-quit? [ & players ] (reduce (fn [a i] (or a (= i 0))) false players))

(defn ttt-loop [players]
  (loop  [board  (vec (repeat 10 " "))]
    (let [choice-x (get-cell-selection players 1 board)
          new-board-1 (make-move choice-x "X" board)
          choice-o (get-cell-selection players 2 new-board-1 (= choice-x 0))
          new-board-2 (make-move choice-o "O" new-board-1)
          winner (winner? new-board-2)
          available-cells (count (open-squares new-board-2))
          ]
      (cond
        (not (= winner " "))
        (do
          (println "\nCongratulations" winner "- you win!")
          (print-board new-board-2))
        (= 0 available-cells)
        (do
          (println "\nDraw!")
          (print-board new-board-2))
        (not (is-quit? choice-x choice-o)) (recur new-board-2)
      ))))

(defn ttt-game []
  (let [players (prompt-player {} "One" :X)
        players-final (prompt-player players "Two" :O)]
    (println (acceptance-string players-final))
    (println)
    (ttt-loop players-final)
  ))

(defn game []
  (print "Would you like to play a game?\n1) Tic Tac Toe\n2) Global Thermonuclear War\n\n] ")
  (flush)
  (let [selection (read-line)]
  (cond
    (= selection "1") (ttt-game)
    (= selection "2") (println "Under construction...maybe next time.\n")
    :else (println "(" selection ") is not an option - have a nice day!\n"))))

(defn -main []
  (game)
)