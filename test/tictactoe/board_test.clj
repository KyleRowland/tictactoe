(ns tictactoe.board-test
  (:require [clojure.test :refer :all]
            [tictactoe.board :refer :all]
            ))
(def all-ones-board " 1 | 1 | 1 \n-----------\n 1 | 1 | 1 \n-----------\n 1 | 1 | 1 \n")


(deftest test-print-row
   (testing "print board's row"
        (let [ board (assoc (vec (repeat 10 1)) 0 0)
              result (with-out-str (print-row board 1))]
          (is (= result " 1 | 1 | 1 \n"))))
   )
(deftest test-print-last-row
  (testing "print last row"
    (let [ board (assoc (vec (repeat 10 1)) 0 0)
          result (with-out-str (print-row board 3))]
      (is (= result " 1 | 1 | 1 \n"))))
  )
(deftest test-print-board
   (testing "printing the full board"
        (let [board (assoc (vec (repeat 10 1)) 0 0)
              result (with-out-str (print-board board))]
          (is (= result all-ones-board)))
        )
   )

(deftest test-winner-check
  (testing "does our check of the winner work"
    (defn quick-board [triple]
      (let [starting-board (vec (repeat 10 " "))
            final-board (assoc starting-board
                                (nth triple 0) "X"
                                (nth triple 1) "X"
                                (nth triple 2) "X") ]
        final-board))

    (let [all-winners [ [1 2 3]
                        [4 5 6]
                        [7 8 9]
                        [1 4 7]
                        [2 5 8]
                        [3 6 9]
                        [1 5 9]
                        [3 5 7]
                       ]
          result (reduce (fn [a i] (and a (= "X" (winner?  (quick-board i))))) true all-winners)]
      (is result)
      )))

(defn three-move-board []
  (let [board (vec (repeat 10  " "))
        result (assoc board 1 "X" 5 "O" 9 "X")]
        result))

(deftest test-get-column
  (testing "the get column should just return a 3 element vecctor representing the column"
    (let [c1 (get-column (three-move-board) 1)
          c2 (get-column (three-move-board) 2)
          c3 (get-column (three-move-board) 3)]
      (is (and (= (nth c1 1) "X") (= (nth c2 2) "O") (= (nth c3 3) "X"))))))

(deftest test-get-all-columns
  (testing "return all columns"
    (let [columns (get-all (three-move-board) get-column)
          c1 (nth columns 1)
          c2 (nth columns 2)
          c3 (nth columns 3)]
      (is (and (= (nth c1 1) "X") (= (nth c2 2) "O") (= (nth c3 3) "X"))))))

(deftest test-saturation
  (testing "return letter if saturated, empty if not"
    (let [triple [" " "X" "X" "X"]]
      (is (=(saturated? triple) "X")))))

(deftest test-saturation-not
  (testing "return letter if saturated, empty if not"
    (let [triple [" " "X" "O" "X"]]
      (is (=(saturated? triple) " ")))))

(deftest test-get-all-rows
  (testing "return all rows"
    (let [rows (get-all (three-move-board) get-row)
          r1 (nth rows 1)
          r2 (nth rows 2)
          r3 (nth rows 3)]
      (is (and (= (nth r1 1) "X") (= (nth r2 2) "O") (= (nth r3 3) "X"))))))