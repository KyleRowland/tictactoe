(ns tictactoe.core-test
  (:require [clojure.test :refer :all]
            [tictactoe.core :refer :all]
            ))


(def test-instructional-board " 1 | 2 | 3 \n-----------\n 4 | 5 | 6 \n-----------\n 7 | 8 | 9 \n")
(def empty-board "   |   |   \n-----------\n   |   |   \n-----------\n   |   |   \n")
(def x-first-square-board " X |   |   \n-----------\n   |   |   \n-----------\n   |   |   \n")
(def x-center-board "   |   |   \n-----------\n   | X |   \n-----------\n   |   |   \n")
(def three-move-board " X |   |   \n-----------\n   | O |   \n-----------\n   |   | X \n")

(deftest test-select-nuke
  (testing "test global thermonuclear war"
    (let [input (str "2\n")
          expected-output (str
                            "Would you like to play a game?\n1) Tic Tac Toe\n2) Global Thermonuclear War\n\n] "
                            "Under construction...maybe next time.\n\n")

          actual-output   (with-out-str
                          (with-in-str input (game)))
          ]
      (is (= actual-output expected-output)))
    ))

(deftest test-select-game
  (testing "set up game"
  (let [input (str "1\nH\nC\n")
        expected-output (str
          "Would you like to play a game?\n1) Tic Tac Toe\n2) Global Thermonuclear War\n\n] "
          "Player One - (H)uman or (C)omputer: "
          "Player Two - (H)uman or (C)omputer: "
          "Fantastic - X will be played by a Human and O will be played by a Computer! Enjoy!")

        actual-output (with-out-str
                      (with-in-str input (game)))
        ]
    (println actual-output)
    (is (.startsWith actual-output expected-output)))
  )
)
(deftest test-select-game-lower
  (testing "set up game"
    (let [input (str "1\nh\nc\n")
          expected-output (str
                            "Would you like to play a game?\n1) Tic Tac Toe\n2) Global Thermonuclear War\n\n] "
                            "Player One - (H)uman or (C)omputer: "
                            "Player Two - (H)uman or (C)omputer: "
                            "Fantastic - X will be played by a Human and O will be played by a Computer! Enjoy!")

          actual-output (with-out-str
                          (with-in-str input (game)))
          ]
      (println actual-output)
      (is (.startsWith actual-output expected-output)))
    )
  )
(deftest test-first-player-quit
  (testing "first player quits without message"
    (let [expected-output (str
            "Player 1 ( X ): Choose a square number ('Q' to quit).\n\n"
            test-instructional-board "\n"
            empty-board "\n\n"
            "] "
            )
      input "Q\n"
      actual-output (with-out-str
                      (with-in-str input (ttt-loop {:X :H
                                                    :O :H})))]
      (print actual-output)
      (is (= actual-output expected-output)))))

(deftest test-human-place-valid-square
  (testing "valid square human"
  (let [expected-output (str
            "Player 1 ( X ): Choose a square number ('Q' to quit).\n\n"
            test-instructional-board "\n"
            empty-board "\n\n"
            "] "
            "Player 2 ( O ): Choose a square number ('Q' to quit).\n\n"
            test-instructional-board "\n"
            x-first-square-board "\n\n"
            "] "
            )
        input "1\nQ\n"
        actual-output (with-out-str
                      (with-in-str input (ttt-loop {:X :H
                                                    :O :H})))]
    (print actual-output)
    (is (= actual-output expected-output)))))

(deftest test-computer-place-valid-square
  (testing "valid square computer"
  (let [players {:X :C
                 :O :H}
        expected-output (str
              x-center-board
              "\n\n"
              "] "
              )
        input "\n\n"
        actual-output   (with-out-str
                        (with-in-str input (ttt-loop players)))]
    (print actual-output)
    (is (.endsWith actual-output expected-output)))))

(deftest test-get-cell-selection-human
  (testing "get cell selection for human"
  (let [
        empty-board-vec (vec (repeat 10 " "))
        players { :X :H
                  :O :C }
        old-func prompt-cell-selection
        mock-prompt-cell-selection (fn [player-number, board]
                                    (if (and (= player-number 1)(= board empty-board-vec)) 42 0))]
    (alter-var-root #'prompt-cell-selection (constantly mock-prompt-cell-selection))
    (def result (get-cell-selection players 1 empty-board-vec))
    (alter-var-root #'prompt-cell-selection (constantly old-func))

    (is (= result 42)))))

(deftest test-get-cell-selection-computer
  (testing "get cell selection for human"
    (let [empty-board-vec (vec (repeat 10 " "))
          players { :X :C
                    :O :C }
          old-func compute-cell-selection
          mock-compute-cell-selection (fn [player-number, board]
                                       (if (and (= player-number 1) (= board empty-board-vec)) 96 0))]
      (alter-var-root #'compute-cell-selection (constantly mock-compute-cell-selection))
      (def result (get-cell-selection players 1 empty-board-vec))
      (alter-var-root #'compute-cell-selection (constantly old-func))

      (is (= result 96)))))


(deftest test-two-humans-two-moves
  (testing "game loop works"
    (let [players {:X :H
                   :O :H}
          expected-output (str three-move-board "\n\n] ")
          input "1\n5\n9\nQ\n"
          actual-output   (with-out-str
                          (with-in-str input (ttt-loop players)))]
      (print actual-output)
      (is (.endsWith actual-output expected-output)))))

(deftest test-two-humans-one-winner
  (testing "game ends with a proper announcement when there's a winner"
  (let [players {:X :H
                 :O :H}
        expected-output (str "Congratulations X - you win!\n")
        input "1\n5\n4\n6\n7\n"
        actual-output   (with-out-str
                          (with-in-str input (ttt-loop players)))]
    (print actual-output)
    (is (.contains actual-output expected-output)))))


(deftest test-two-computers-draw
  (testing "game ends with a proper announcement when there's a draw!"
    (let [players {:X :C
                   :O :C}
          expected-output (str "Draw!\n")
          actual-output   (with-out-str (ttt-loop players))]
      (print actual-output)
      (is (.contains actual-output expected-output)))))