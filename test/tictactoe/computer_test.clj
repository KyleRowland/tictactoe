(ns tictactoe.computer-test
  (:require [clojure.test :refer :all]
            [tictactoe.computer :refer :all]
            [tictactoe.board :refer :all]
            ))
(defn quick-board [triple who]
  (let [starting-board (vec (repeat 10 " "))
        final-board (assoc starting-board
                      (nth triple 0) who
                      (nth triple 1) who
                      (nth triple 2) who) ]
                      final-board))

(defn quick-add-board [board triple who]
  (let [final-board (assoc board
                      (nth triple 0) who
                      (nth triple 1) who
                      (nth triple 2) who) ]
    final-board))

(deftest test-open-squares
  (testing "we can find the list of open squares"
    (let [board (quick-board [1 8 9] "X")
          result (open-squares board)]
      (is (= result [2 3 4 5 6 7])))))


(deftest test-computer-move-win
  (testing "we have the possibility to win - let's take it"
  (let [board (quick-board [1 8 9] "X")
        move  (computer-move "X" board)]
    (is (= move 5)))))

(deftest test-computer-block-loss
  (testing "possible opponent win is blocked"
    (let [board (quick-board [1 1 3] "O")
          move  (computer-move "X" board)]
      (is (= move 2)))))


(deftest test-computer-avoid-fork
  (testing "avoid fork creation"
    (let [board (quick-board [1 1 9] "X")
          final-board (quick-add-board board [5 5 5] "O")
          move  (computer-move "O" final-board)]
      (is (or (= move 2) (= move 4) (= move 6) (= move 8))))))

(deftest test-computer-take-center
  (testing "take center if available"
    (let [board (quick-board [1 1 1] "X")
          move  (computer-move "O" board)]
      (is (or (= move 5))))))

